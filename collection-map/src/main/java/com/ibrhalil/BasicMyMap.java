package com.ibrhalil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class BasicMyMap {
    private Map<String, Object> myMap = new HashMap<>();

    public void addGrade(String key, Object value) {
        myMap.put(key, value);
    }

    public void printAllGrade() {
        Set set = myMap.entrySet();
        Iterator iterator = set.iterator();

        while(iterator.hasNext()) {
            Map.Entry item = (Map.Entry) iterator.next();
            System.out.println("key : ".concat(item.getKey().toString())
                    .concat(" - ")
                    .concat("value : ")
                    .concat(item.getValue().toString()));
        }
        System.out.println("______________ size: ".concat(String.valueOf(myMap.size())));
    }


    public void changeGrade(String key, Object value) {
        //myMap.put(key, value); don't use this
        myMap.replace(key, value);
    }

    public void notAvailableAdd(String key, Object value) {
        myMap.putIfAbsent(key, value);
    }

    public void showGrade(String key) {
        System.out.println(key.concat(" -> ").concat(myMap.get(key).toString()));
    }

    public void deleteGrade(String key) {
        myMap.remove(key);
    }
}
