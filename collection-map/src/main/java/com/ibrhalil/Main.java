package com.ibrhalil;

public class Main {
    public static void main(String[] args) {
        BasicMyMap mathGrades = new BasicMyMap();

        mathGrades.addGrade("ali",85);
        mathGrades.addGrade("veli",77);
        mathGrades.addGrade("ayşe",70);
        mathGrades.addGrade("fatma",55);
        mathGrades.addGrade("hayriye",99);

        mathGrades.printAllGrade();

        mathGrades.changeGrade("halil",100);
        mathGrades.changeGrade("veli",53);
        mathGrades.showGrade("veli");

        mathGrades.notAvailableAdd("ali",10);
        mathGrades.notAvailableAdd("ramazan",40);

        mathGrades.printAllGrade();

        mathGrades.deleteGrade("ayşe");

        mathGrades.printAllGrade();

    }
}